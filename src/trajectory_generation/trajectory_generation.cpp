#include "trajectory_generation.h"

Polynomial::Polynomial(){};

Polynomial::Polynomial(const double &piIn, const double &pfIn, const double & DtIn){
  //TODO initialize the object polynomial coefficients
 a={piIn,0,0,10,-15,6};
 pi=piIn;
 pf=piIn;
 Dt=DtIn;
};



void          Polynomial::update(const double &piIn, const double &pfIn, const double & DtIn){
  //TODO update polynomial coefficients
pi=piIn;
pf=pfIn;
Dt=DtIn;


};


const double  Polynomial::p     (const double &t){
  //TODO compute position
double p=a[0]+a[1]*(pf-pi)*pow((t/Dt),1)+a[2]*(pf-pi)*pow((t/Dt),2)+a[3]*(pf-pi)*pow((t/Dt),3)+a[4]*pow((t/Dt),4)+a[5]*(pf-pi)*pow((t/Dt),5);
  
  return p;

  
};

const double  Polynomial::dp    (const double &t){
  //TODO compute velocity
double p1= (a[1]/Dt)+(2*a[2]/Dt)+(3*a[3]/Dt)*pow(t/Dt,2)+(4*a[4]/Dt)*pow(t/Dt,3)+(5*a[5]/Dt)*pow(t/Dt,4);
  return p1;
};

Point2Point::Point2Point(const Eigen::Affine3d & X_i, const Eigen::Affine3d & X_f, const double & DtIn){
 
}
Dt=DtIn;
  Eigen::Matrix3d Ri = X_i.linear().matrix();
  Eigen::Matrix3d Rf = X_f.linear().matrix();
  Eigen::Vector3d Ti = X_i.translation();
  Eigen::Vector3d Tf = X_f.translation();
 //TODO initialize object and polynomials
  polx = Polynomial(Ti.translation()(0),Tf.translation()(0),Dt);
  poly = Polynomial(Ti.translation()(1),Tf.translation()(1),Dt);
  polz = Polynomial(Ti.translation()(2),Tf.translation()(2),Dt);
  Eigen::Matrix3d Rot=Rf*Ri.transpose(); 
  Eigen::AngleAxisd R(Rot);
  axis = R.axis();
  double angle = R.angle();
  pol_angle = Polynomial(0,angle,Dt)


Eigen::Affine3d Point2Point::X(const double & time){ 
    //TODO compute cartesian position
  Eigen::Affine3d X;                                         
  Eigen::Vector3d position(polx.p(time), poly.p(time), polz.p(time));
  X.translation()=position;
    //compute joint position
  Eigen::AngleAxisd Rotation = Eigen::AngleAxisd(pol_angle.p(time), axis);
  X.linear().matrix() = Rotation.matrix();

  return X;
}

Eigen::Vector6d Point2Point::dX(const double & time){
  //TODO compute cartesian velocity
  Eigen::Vector3d carvelocity;
  Eigen::Vector3d joint;

  carvelocity=(polx.dp(time), poly.dp(time), polz.dp(time));
  joint=(pol_angle.dp(time)*axis);
  Eigen::Vector6d dX;
  dX(0:2) = dX_cart;
  dX(3:5) = dX_joint;
  return dX;
}