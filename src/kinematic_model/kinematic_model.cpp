#include "kinematic_model.h"

RobotModel::RobotModel(const std::array<double,7> &aIn, const std::array<double,7> &dIn, const std::array<double,7> &alphaIn):
  a     {0,0,0,0,0,0,0},
  d     {0,0,0,0,0,0,0},
  alpha {0,0,0,0,0,0,0}
{};

void RobotModel::FwdKin(Eigen::Affine3d &xOut, Eigen::Matrix67d &JOut, const Eigen::Vector7d & qIn){
  // TODO Implement the forward and differential kinematics
}